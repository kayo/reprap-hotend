module screw_model(
    screw_dia=2.0,
    screw_len=6.0,
    
    head_dia=3.5,
    head_len=1.2,
){
    union(){
        /* screw */
        translate([0, 0, -$fix])
        cylinder(r=screw_dia/2, h=screw_len+$fix);
        
        /* head */
        translate([0, 0, -head_len])
        cylinder(r=head_dia/2, h=head_len);
    }
}

module mount_screw(
    screw_dia=2.0,
    screw_len=6.0,
    screw_out=2.0,
    
    head_dia=3.5,
    head_len=1.2,
    head_out=2,
){
    #screw_model(screw_dia, screw_len, head_dia, head_len);
    screw_model(screw_dia, screw_len+screw_out, head_dia, head_len+head_out);
}
