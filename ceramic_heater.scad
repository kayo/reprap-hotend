module ceramic_heater_mount(
    dia=6,
    len=20,
){
    wire_dia=dia/2;
    wire_len=len*2;

    translate([0, 0, -$fix])
    cylinder(r=dia/2, h=len+$fix*2);
    
    %for(a=[0, 180])
    rotate([0, 0, a+30])
    translate([wire_dia/2, 0, len])
    rotate([5, 0, 0])
    cylinder(r=wire_dia/2, h=wire_len);
}
