use <ceramic_heater.scad>
use <mount_screw.scad>
use <extern/PrusaMendel/source/x-carriage.scad>

module plate_block(
    width=20,
    length=46,
    thickness=6,
    
    hole_dia=8,
    
    mount_offset=18,
    mount_repeat=3,
    mount_dia=3,
    
    screw_offset=10,
    
    hotend_screw_offset=[-7, 10, 3],
){
    difference(){
        *minkowski(){
            translate([-$fix/2, -length/2+width/2, 0])
            cube([$fix, length-width, thickness-$fix]);
            
            cylinder(r=width/2, h=$fix);
        }
        
        translate([-width/2, -length/2, 0])
        cube([width, length, thickness]);
        
        translate([0, 0, -$fix])
        cylinder(r=hole_dia/2, h=thickness+$fix*2);
        
        for(m=[0, 1])
        mirror([0, m, 0])
        translate([0, mount_offset, -$fix])
        for(i=[0:mount_repeat-1])
        translate([-width/(mount_repeat+0.5)*(i-1), 0, 0])
        cylinder(r=mount_dia/2, h=thickness+$fix*2);

        /* build screws */
        for(m=[0, 1], n=[0, 1])
        mirror([m, 0, 0])
        mirror([0, n, 0])
        translate([-width/2, screw_offset, thickness/2])
        rotate([0, 90, 0])
        mount_screw();
        
        /* hotend screws */
        translate([hotend_screw_offset[0], 0, hotend_screw_offset[2]])
        rotate([0, 180, 0])
        mount_screw();
        
        for(m=[0, 1])
        mirror([0, m, 0])
        translate([0, hotend_screw_offset[1], hotend_screw_offset[2]])
        rotate([0, 180, 0])
        mount_screw();
    }
}

module plate_mount(
    width=20,
    length=60,
    thickness=6,
    
    hole_dia=8,
    
    mount_offset=25,
    mount_dia=4,
    /*
    heater_wire_offset=[6, 5],
    heater_wire_dia=6,

    sensor_wire_offset=7,
    sensor_wire_dia=3,
    */
    screw_offset=10,
){
    difference(){
        *minkowski(){
            translate([-$fix/2, -length/2+width/2, 0])
            cube([$fix, length-width, thickness-$fix]);
            
            cylinder(r=width/2, h=$fix);
        }

        translate([-width/2, -length/2, 0])
        cube([width, length, thickness]);
        
        translate([0, 0, -$fix])
        cylinder(r=hole_dia/2, h=thickness+$fix*2);
        
        for(m=[0, 1])
        mirror([0, m, 0])
        translate([0, mount_offset, -$fix])
        cylinder(r=mount_dia/2, h=thickness+$fix*2);
        /*
        for(m=[0, 1])
        mirror([0, m, 0])
        translate([heater_wire_offset[0], heater_wire_offset[1], -$fix])
        cylinder(r=heater_wire_dia/2, h=thickness+$fix*2);

        translate([sensor_wire_offset, 0, -$fix])
        cylinder(r=sensor_wire_dia/2, h=thickness+$fix*2);
        */
        for(m=[0, 1], n=[0, 1])
        mirror([m, 0, 0])
        mirror([0, n, 0])
        translate([-width/2, screw_offset, thickness/2])
        rotate([0, 90, 0])
        mount_screw();
    }
}

module tube(
    out_dia=8,
    hole_dia=3,
    
    height=30,
){
    difference(){
        cylinder(r=out_dia/2, h=height);

        translate([0, 0, -$fix])
        cylinder(r=hole_dia/2, h=height+$fix*2);
    }
}

module block(
    width=20,
    length=25,
    height=10,
    
    ring_dia=12,
    ring_depth=0,
    
    tube_dia=8,
    tube_depth=0,
    
    hole_dia=3,
    
    nozzle_dia=12,
    nozzle_depth=0,
    nozzle_head=2,
    
    heater_offset=[-10, 5.5],
    screw_offset=[5, 10, 2.5],
    
    sensor_dia=2.5,
    sensor_len=7.5,
    sensor_offset=2,
    
    hotend_screw_offset=[-7, 10, 8],
    
    clearance=1,
){
    difference(){
        translate([-width/2, -length/2, -height/2])
        cube([width, length, height]);
        
        /* teflon tube */
        translate([0, 0, height/2-ring_depth+$fix])
        cylinder(r=ring_dia/2, h=ring_depth+$fix);
        
        translate([0, 0, height/2-tube_depth-ring_depth])
        cylinder(r=tube_dia/2, h=tube_depth+ring_depth+$fix);
        
        /* filament hole */
        translate([0, 0, -height/2-$fix])
        cylinder(r=hole_dia/2, h=height+$fix*2);

        /* nozzle mount */
        translate([0, 0, -height/2-$fix])
        cylinder(r=nozzle_dia/2, h=nozzle_depth+$fix);

        /* screws */
        for(n=[0, 1])
        //rotate([0, 0, 45])
        mirror([n, 0, 0])
        translate([(nozzle_dia+hole_dia)/4, 0, -height/2-nozzle_head])
        mount_screw(screw_out=0.5);
        
        /* sensors */
        for(m=[0, 1])
        //mirror([m, 0, 0])
        translate([width/2+$fix, 0, sensor_offset])
        rotate([0, -90, 0])
        cylinder(r=sensor_dia/2, h=sensor_len+$fix);
        
        for(m=[0, 1])
        mirror([0, m, 0]){
            /* screws */
            for(n=[0, 1])
            mirror([n, 0, 0])
            translate([screw_offset[0], screw_offset[1], -height/2+screw_offset[2]])
            mount_screw();
            
            /* heaters */
            translate([heater_offset[0], heater_offset[1], 0])
            rotate([0, 90, 0])
            ceramic_heater_mount();

            /* clearance */
            translate([-width/2-$fix, heater_offset[1], -clearance/2])
            cube([width+$fix*2, length-heater_offset[1]+$fix, clearance]);
        }
        
        /* hotend screws */
        translate([hotend_screw_offset[0], 0, hotend_screw_offset[2]])
        rotate([0, 180, 0])
        mount_screw();
        
        for(m=[0, 1])
        mirror([0, m, 0])
        translate([0, hotend_screw_offset[1], hotend_screw_offset[2]])
        rotate([0, 180, 0])
        mount_screw();
    }
}

module nozzle(
    hole_dia=3,
    hole_end=0.3,
    
    head_dia=12,
    head_depth=1,
    head_height=2,
    
    out_dia=4,
    out_end=1,
    height=3,
    
    screw_offset=1,
){
    difference(){
        union(){
            /* head */
            translate([0, 0, -head_height])
            cylinder(r=head_dia/2, h=head_depth+head_height);
            
            /* end */
            translate([0, 0, -head_height-height])
            cylinder(r1=out_end/2, r2=out_dia/2, h=height+$fix);
        }
        
        /* crater */
        translate([0, 0, $fix])
        cylinder(r=hole_dia/2, h=head_depth+$fix*2);
        
        translate([0, 0, -hole_dia/3+$fix])
        cylinder(r1=0, r2=hole_dia/2, h=hole_dia/3);
        
        /* crater 2 */
        translate([0, 0, -head_height-head_depth])
        cylinder(r=hole_dia/4, h=head_height+head_depth+$fix*2);
        
        translate([0, 0, -head_height-head_depth-hole_dia/6+$fix])
        cylinder(r1=0, r2=hole_dia/4, h=hole_dia/6);
        
        /* channel */
        translate([0, 0, -head_height-height-head_depth-$fix])
        cylinder(r=hole_end/2, h=height+head_height+head_depth+$fix*2);
        
        /* screws */
        for(n=[0, 1])
        //rotate([0, 0, 45])
        mirror([n, 0, 0])
        translate([(head_dia+hole_dia)/4, 0, -screw_offset])
        mount_screw();
    }
}

module design(){
    translate([0, 6, 0])
    rotate([0, 180, 0])
    xcarriage(true);

    translate([0, 0, -20])
    color("Peru")
    for(n=[3:4:20])
    translate([0, 0, n])
    plate();
    
    translate([0, 0, -25])
    color([.9, .9, .9])
    tube();
    
    translate([0, 0, -25])
    color("Tan"){
        plate_block(thickness=6);
        
        translate([0, 0, 14])
        plate_mount(thickness=6);
    }
    
    *translate([0, 0, -30])
    block();
    
    translate([0, 0, -36])
    nozzle();
}

module ofour(){
    difference(){
        child(0);
        
        //rotate([0, 0, 180])
        translate([0, 0, -50])
        cube([100, 100, 100]);
    }
}

module assembly(){
    translate([0, 6, 0])
    rotate([0, 180, 0])
    xcarriage(true);
    
    translate([0, 0, -5]){
        translate([0, 0, -20])
        for(n=[4:3:20])
        translate([0, 0, n])
        plate(length=40);
        
        translate([0, 0, -25]){
            color([.9, .9, .9])
            render()
            ofour()
            tube();
            
            color("Tan")
            translate([0, 0, 1.5])
            render()
            ofour()
            plate_block(thickness=6);
            
            color("Tan")
            translate([0, 0, 14])
            render()
            ofour()
            plate_mount(thickness=6);
        }
        
        translate([0, 0, -30])
        color("Turquoise")
        render()
        ofour()
        block();

        translate([0, 0, -36])
        color("LightCyan")
        render()
        ofour()
        nozzle();
    }
}

module drawing(){
    $fn=8;
    
    projection(true){
        
    for(d=[[0, $fix], [30, 3], [60, 5], [90, 10-$fix]])
    translate([d[0], 0, d[1]-5])
    block();
    
    for(d=[[0, $fix], [30, 3], [60, 5], [90, 10-$fix]])
    translate([d[0], 30, d[1]])
    rotate([0, 90, 0])
    block();

    translate([0, 60, -15])
    tube();
    
    translate([10, 60, 0])
    rotate([0, 90, 0])
    tube();

    translate([0, 90, -15])
    nozzle();
    
    translate([10, 90, 0])
    rotate([0, 90, 0])
    nozzle();

    translate([30, -30, 0])
    rotate([0, 0, 90]){
        plate_mount();

        translate([-20, 0, 0])
        rotate([0, 90, 0])
        plate_mount();

        translate([-30, 0, -10+$fix])
        rotate([0, 90, 0])
        plate_mount();
    }

    translate([30, -80, 0])
    rotate([0, 0, 90]){
        plate_block();

        translate([-20, 0, 0])
        rotate([0, 90, 0])
        plate_block();

        translate([-30, 0, -10+$fix])
        rotate([0, 90, 0])
        plate_block();
    }
    
    }
}

$fix=0.01;
$fn=100;

*difference(){
    design();
    
    //rotate([0, 0, 180])
    translate([0, 0, -100])
    cube([100, 100, 200]);
}

assembly();

*drawing();
